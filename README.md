# Reports-Platform

project in development

# Add in project composer.json for local symlink development

#### Change url by need
```sh
"minimum-stability": "dev",
  "repositories": [
    {
      "type": "path",
      "url": "../ReportsPlatform",
      "options": {
        "symlink": true
      }
    }
  ]
```
#### Useful links

`Class and methods design` : <http://bestpractices.thecodingmachine.com/php/design_beautiful_classes_and_methods.html>

`PHP PDO` : <https://phpdelusions.net/pdo#dsn>

`README Editor` : <https://pandao.github.io/editor.md/en.html>