<?php
    $index = bin2hex(openssl_random_pseudo_bytes(128));
?>

<div class="row" style="margin-bottom: 15px" id="select-row-<?= $index; ?>">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-4 form-inline">
                <label for="select-table-<?= $index; ?>">Table</label>
                <select class="form-control" id="select-table-<?= $index; ?>" style="width: 30%">
                    <option></option>
                    <?php foreach ($tables as $table): ?>
                        <option> <?= $table; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-4 form-inline">
                <label for="select-function-<?= $index; ?>">Function</label>
                <select class="form-control" disabled id="select-function-<?= $index; ?>" style="width: 30%">
                    <option></option>
                    <?php foreach ($queryBuilderConfig['select'] as $label => $options): ?>
                        <optgroup label="<?= $label; ?>">
                            <?php foreach ($options as $option): ?>
                                <option><?= $option; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-4 form-inline">
                <label for="select-column-<?= $index; ?>">Column</label>
                <select class="form-control" disabled id="select-column-<?= $index; ?>" style="width: 30%">
                    <option></option>
                </select>
            </div>
        </div>
    </div>
</div>