<script type='text/javascript' src="<?= JQUERY_3_3_1; ?>/js/jquery-3.3.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>
                Configure Report
            </h4>
        </div>
        <div class="panel-body">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        General
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-6 form-inline">
                            <label for="reportName">Report Name</label>
                            <input class="form-control" type="text" id="reportName">
                        </div>
<!--                        <div class="col-md-6 form-inline">-->
<!--                            <label for="mainTable">Main Table</label>-->
<!--                            <input class="form-control" type="text" id="mainTable">-->
<!--                        </div>-->
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>
                        Select
                    </h4>
                </div>
                <div class="panel-body" id="select-panel-body">
                    <?php include('selectRow.php') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    // var numberOfRows = 0;



    $(document).ready(function () {

        var columnsByTables = <?= json_encode($columnsByTables); ?>;

        // $('#select-panel-body').html($('#select-panel-body').html().replace(/replaceIndex/g, numberOfRows));

        $('[id^="select-table-"]').select2({
        //$('#select-table-' + '<?//= $index; ?>//').select2({
            allowClear: true,
            placeholder: ''
        });
        $('[id^="select-function-"]').select2({
        //$('#select-function-' + '<?//= $index; ?>//').select2({
            allowClear: true,
            placeholder: ''
        });
        $('[id^="select-column-"]').select2({
        //$('#select-column-' + '<?//= $index; ?>//').select2({
            allowClear: true,
            placeholder: ''
        });

        $(document).on('change', '[id^="select-table-"]', function () {
        // $('[id^="select-table-"]').on('change', function () {

            var actionRow = $(this).attr('id').split('select-table-')[1];

            if ($(this).val() === '')
            {
                //empty and disable the function and select from current row

                $('#select-function-' + actionRow).empty().trigger("change");
                $('#select-function-' + actionRow).attr('disabled', 'disabled');
                $('#select-column-' + actionRow).empty().trigger("change");
                $('#select-column-' + actionRow).attr('disabled', 'disabled');

                //remove the next row since the user will have the current one free to fill

                // actionRow += 1;
                // console.log($('#select-row-' + actionRow).siblings().last());
                var nextRow = $('#select-row-' + actionRow).siblings(":last").attr('id').split('select-row-')[1];
                $('#select-row-' + nextRow).remove();
            }
            else
            {
                //enable the function and column select from current row
                $('#select-function-' + actionRow).prop('disabled', false);
                $('#select-column-' + actionRow).prop('disabled', false);

                //empty the column
                $('#select-column-' + actionRow).empty().trigger("change");

                var selectedTable = $(this).val();
                var columnsByTable = columnsByTables[selectedTable];

                // Append empty option to columns select
                var optionData = {
                    id: '',
                    text: ''
                };

                //append empty option to columns so we can empty it when user empties whole row
                var option = new Option(optionData.text, optionData.id, false, false);
                $('#select-column-' + actionRow).append(option).trigger('change');

                //append columns by table selected
                for (var index in columnsByTable)
                {
                    var optionData = {
                        id: index,
                        text: columnsByTable[index],
                    };

                    option = new Option(optionData.text, optionData.id, false, false);
                    $('#select-column-' + actionRow).append(option).trigger('change');
                }

                //append next row to be filled
                appendSelectRow();
            }
        });
    });

    function appendSelectRow()
    {
        // numberOfRows++;

        var selectRow = <?php
//            $index++;
            ob_start();
            include('selectRow.php');
            $selectRow = ob_get_clean();
            echo json_encode($selectRow);
            ?>;
        $('#select-panel-body').append(selectRow);

        // $('#select-panel-body').html($('#select-panel-body').html().replace(/replaceIndex/g, numberOfRows));

        var lastIndex = $('#select-panel-body').children('.row:last').attr('id').split('select-row-')[1];

        //$('#select-table-' + '<?//= $index; ?>//').select2({
        // $('[id^="select-table-"]').select2({
        $('#select-table-' + lastIndex).select2({
            allowClear: true,
            placeholder: ''
        });
        //$('#select-function-' + '<?//= $index; ?>//').select2({
        // $('[id^="select-function-"]').select2({
        $('#select-function-' + lastIndex).select2({
            allowClear: true,
            placeholder: ''
        });
        //$('#select-column-' + '<?//= $index; ?>//').select2({
        // $('[id^="select-column-"]').select2({
        $('#select-column-' + lastIndex).select2({
            allowClear: true,
            placeholder: ''
        });
    }
</script>