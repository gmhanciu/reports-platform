<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/vuex@3.1.0/dist/vuex.js"></script>
    <script type='text/javascript' src="<?= JQUERY_3_3_1; ?>/js/jquery-3.3.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<body>

<div id="app">
    <div class="row" style="margin-bottom: 15px" id="select-row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-4 form-inline">
                    <select-table></select-table>
                </div>
                <div class="col-md-4 form-inline">
                    <select-function></select-function>
                </div>
                <div class="col-md-4 form-inline">
                    <select-column></select-column>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var tables = <?= json_encode($tables); ?>;
    var columnsByTables = <?= json_encode($columnsByTables); ?>;
    var queryBuilder = <?= json_encode($queryBuilderConfig); ?>;

    var SelectTable = Vue.component('SelectTable', {
        data: function (){
            return {
                tables: tables,
                selectedTable: '',
            }
        },
        methods: {
            emitTableChange() {
                var payload = {
                    table: this.selectedTable,
                    columnsByTables: columnsByTables,
                };

                store.dispatch('tableChanged', payload);
            }
        },
        template:
            `<div id="select-table-div">` +
            `                    <label for="select-table">Table</label>\n` +
            `                    <select class="form-control" v-model="selectedTable" @change="emitTableChange" ref="select-table" style="width: 30%">\n` +
            `                        <option></option>\n` +
            `                        <option v-for="table in tables" :value="table">{{table}}</option>\n` +
            `                    </select>` +
            `</div>`
    });

    var SelectFunction = Vue.component('select-function', {
        data: function () {
            return {
                queryBuilder: queryBuilder['select'],
                selectedFunction: '',
            }
        },
        computed: {
            disabled: {
                get: function () {
                    this.selectedFunction = '';
                    return store.getters.getDisableSelects;
                },
                set: function (value) {}
            },
        },
        template:
            `<div id="select-function-div">` +
            `                    <label for="select-function">Function</label>` +
            `                    <select class="form-control" v-model="selectedFunction" :disabled="disabled" ref="select-function" style="width: 30%">\n`+
            `                        <option></option>\n`+
            `                            <optgroup v-for="(options, label) in queryBuilder" :label="label">\n`+
            `                                    <option v-for="option in options" :value="option">{{option}}</option>\n`+
            `                            </optgroup>\n`+
            `                    </select>` +
            `</div>`
    });

    var SelectColumn = Vue.component('select-column', {
        data: function () {
            return {
                columnsByTables: columnsByTables,
                selectedColumn: '',
            }
        },
        computed: {
            disabled: {
                get: function () {
                    this.selectedColumn = '';
                    return store.getters.getDisableSelects;
                },
                set: function (value) {}
            },
            columns: {
                get: function () {
                    return store.getters.getColumns;
                },
                set: function () {},
            }
        },
        template:
            `<div id="select-column-div">` +
            `                    <label for="select-column">Column</label>\n` +
            `                    <select class="form-control" :disabled="disabled" v-model="selectedColumn" ref="select-column" style="width: 30%">\n` +
            `                        <option></option>\n` +
            `                        <option v-for="column in columns" :value="column">{{column}}</option>\n` +
            `                    </select>` +
            `</div>`
    });

    const store = new Vuex.Store({
        state: {
            table: '',
            disableSelects: true,
            columns: {},
        },
        getters: {

            getTable: state => {
                return state.table;
            },
            getDisableSelects: state => {
                return state.disableSelects;
            },
            getColumns: state => {
                return state.columns;
            },
        },
        mutations: {
            tableChanged (state, payload) {
                state.table = payload.table;
                state.disableSelects = (payload.table === '');
                state.columns = (payload.table === '') ? {} : payload.columnsByTables[payload.table];
            }
        },
        actions: {
            tableChanged (context, payload)
            {
                context.commit('tableChanged', payload);
            }
        }
    });

    var app = new Vue({
        el: '#app',
        components: {
            'select-table': SelectTable,
            'select-function': SelectFunction,
            'select-column': SelectColumn,
        }
    });
</script>

</body>
</html>