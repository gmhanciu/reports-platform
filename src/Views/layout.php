<?php

return [
    'JQUERY' => [
        '3.3.1' => [
            "<script type='text/javascript' src=" . JQUERY_3_3_1 . "/js/jquery-3.3.1.min.js></script>",
        ]
    ],
    'BOOTSTRAP' => [
        '3.4.1' => [
            "<script type='text/javascript' src=" . BOOTSTRAP_3_4_1 . "/js/bootstrap.js></script>",
            "<script type='text/javascript' src=" . BOOTSTRAP_3_4_1 . "/js/npm.js></script>",
            "<link rel='stylesheet' type='text/css' href=" . BOOTSTRAP_3_4_1 . "/css/bootstrap.css>",
            "<link rel='stylesheet' type='text/css' href=" . BOOTSTRAP_3_4_1 . "/css/bootstrap-theme.css>",
        ]
    ],
];