<?php

return [
    'BOOTSTRAP' => [
        '3.4.1' => true,
    ],
    'JQUERY' => [
        '3.3.1' => true,
    ],
];