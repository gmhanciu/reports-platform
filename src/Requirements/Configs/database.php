<?php

return [
    'DB_DATABASE_DRIVER' => '',
    'DB_HOST' => '',
    'DB_PORT' => '',
    'DB_DATABASE' => '',
    'DB_CHARSET' => 'utf8mb4',
    'DB_USERNAME' => '',
    'DB_PASSWORD' => '',
    'OPTIONS' => [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ]
];

