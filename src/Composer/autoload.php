<?php

use GMHanciu\ReportsPlatform\Helpers\File;


if (stripos(PHP_OS, 'WIN') !== false)
{
    $tempPath = 'D:/Licenta/LaravelTest/vendor/gmhanciu/reports-platform/src';
}
else if (stripos(PHP_OS, 'LINUX') !== false)
{
    $tempPath = '/home/gmhanciu/web/live/vendor/gmhanciu/reports-platform/src';
}
else
{
//    define('PACKAGE_ROOT_FOLDER', dirname(dirname(dirname(__FILE__))));
}

require_once $tempPath . '/Configs/constants.php';

//$src = $tempPath . '/Requirements/Configs';
$src = PACKAGE_REQUIREMENTS_CONFIG_FOLDER;
$dist = PROJECT_ROOT_FOLDER . '/ReportsPlatform/config';
File::copyFiles($src, $dist);
File::createDir(PROJECT_REPORTS_CONFIG_FOLDER);

//$src = $tempPath . '/Requirements/Assets';
$src = PACKAGE_REQUIREMENTS_ASSETS_FOLDER;
$dist = PROJECT_PUBLIC_FOLDER . '/ReportsPlatform/';
File::copyFiles($src, $dist);

$src = PACKAGE_CONFIG_VIEWS_FOLDER . '/Create';
$file = 'create.js';
$dist = PROJECT_PUBLIC_FOLDER . '/ReportsPlatform/views/js/config/create/';
File::copyFile($src, $dist, $file);
