<?php

namespace GMHanciu\ReportsPlatform\Helpers;


class File
{
    public static function varExportMyWay($var, $indent="") {
        switch (gettype($var)) {
            case "string":
                return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
            case "array":
                $indexed = array_keys($var) === range(0, count($var) - 1);
                $r = [];
                foreach ($var as $key => $value) {
                    $r[] = "$indent    "
                        . ($indexed ? "" : self::varExportMyWay($key) . " => ")
                        . self::varExportMyWay($value, "$indent    ");
                }
                return "[\r\n" . implode(",\r\n", $r) . "\r\n" . $indent . "]";
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, TRUE);
        }
    }

    public static function copyFile($src, $dst, $file)
    {
        $oldUMask = umask(0);
        @mkdir($dst, 0777, true);
        if (is_file($src . '/' . $file))
        {
//            if (!file_exists($dst . '/' . $file))
//            {
                copy($src . '/' . $file,$dst . '/' . $file);
//            }
        }
        umask($oldUMask);
    }

    public static function copyFiles($src, $dst) {
        $dir = opendir($src);

        $oldUMask = umask(0);
        @mkdir($dst, 0777, true);

        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    self::copyFiles($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    if (file_exists($dst . '/' . $file))
                    {
                        //Regenerating Configs is only available for configs, otherwise just copy
                        //or don't copy files

                        if (is_file(PACKAGE_REQUIREMENTS_CONFIG_FOLDER . "/" . $file))
                        {
                            $projectFileData = require $dst . '/' . $file;
                            $packageFileData = require $src . '/' . $file;
                            if (!Arrays::hasAllKeys($projectFileData, $packageFileData))
                            {
                                Arrays::addMissingKeys($projectFileData, $packageFileData);
                                $finalConfigText = self::varExportMyWay($projectFileData, "");
                                //                        dump(file_get_contents($dst . '/' . $file));
                                //                        dd(self::var_export54($projectFileData, ""), var_export($projectFileData, true));

                                $finalFileContent = "<?php\r\n
return " . $finalConfigText . ";";
                                //                            $finalFileContent = "<?php\r\n
                                //$" . "config = " . $finalConfigText . ";
                                //return $" . "config;";

                                //                            dd($finalFileContent);
                                file_put_contents($dst . '/' . $file, $finalFileContent);
                            }
                        }
                    }
                    else
                    {
                        copy($src . '/' . $file,$dst . '/' . $file);
                    }
                }
            }
        }
        closedir($dir);
        umask($oldUMask);
    }

    public static function createDir($dst)
    {
        $oldUMask = umask(0);
        @mkdir($dst, 0777, true);
        umask($oldUMask);
    }
}