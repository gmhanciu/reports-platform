<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 24-Feb-19
 * Time: 18:29
 */

namespace GMHanciu\ReportsPlatform\Helpers;


class Arrays
{
    public static function hasAllKeys($data, $required)
    {
        foreach ($required as $key => $value) {
            if (!isset($data[$key])/* && $data[$key] === $value */) {
                return false;
            }
            if (is_array($data[$key]) && false === self::hasAllKeys($data[$key], $value)) {
                return false;
            }
        }
        return true;
    }

//    public static function getMissingKeys($data, $required, &$missingKeys)
    public static function addMissingKeys(&$data, $required)
    {
        foreach ($required as $key => $value) {
            if (!isset($data[$key])/* && $data[$key] === $value */) {
                $data[$key] = $value;
            }
            if (is_array($data[$key])) {
                self::addMissingKeys($data[$key], $value);
            }
        }
    }



}