<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 10-Mar-19
 * Time: 20:26
 */

namespace GMHanciu\ReportsPlatform\Report;


class View
{

    private $config = null;
    private $configFilename = 'view';

    private $includes = [];

    private $view;

    public function __construct(String $filename, array $params = [])
    {
        if ($this->configExists($this->getConfigFilename()))
        {
            $fullConfigPath = $this->fullPath($this->getConfigFilename(), 'config');
            $config = require $fullConfigPath;
            $this->setConfig($config);

            $fullLayoutPath = PACKAGE_VIEWS_FOLDER . "/layout.php";

            if (file_exists($fullLayoutPath))
            {
                $layouts = require $fullLayoutPath;
//                $this->setIncludes($layouts);
                $this->parseConfig();
            }

            $this->build($filename, $params);
        }
    }

    private function fullPath(String $filename, String $type): String
    {
        switch ($type)
        {
            case 'config':
                return PROJECT_CONFIG_FOLDER . "/" . $filename . '.php';
                break;
        }
    }

    private function configExists(String $filename): bool
    {
        $file = $this->fullPath($filename, 'config');

        return is_file($file);
    }

    /**
     * @return null
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param null $config
     */
    public function setConfig($config): void
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getConfigFilename(): string
    {
        return $this->configFilename;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->view;
    }

    /**
     * @param mixed $view
     */
    public function setView($view): void
    {
        $this->view = $view;
    }

    /**
     * @param array $includes
     */
    public function setIncludes(array $includes): void
    {
        $this->includes = $includes;
    }

    /**
     * @return array
     */
    public function getIncludes(): array
    {
        return $this->includes;
    }

    public function unsetIncludes(string $tool, string $version = null): void
    {
        $temp = $this->getIncludes();

        unset($temp[$tool][$version]);

        if (empty($temp[$tool]))
        {
            unset($temp[$tool]);
        }

        $this->setIncludes($temp);
    }

    private function parseConfig()
    {
        foreach ($this->getConfig() as $tool => $versions)
        {
            if (is_array($versions))
            {
                foreach ($versions as $version => $useTool)
                {
                    if ($useTool)
                    {
                        $version = str_replace('.', '_', $version);
                        $include = $tool . "_" . $version;

                        $temp = $this->getIncludes();

                        $temp[] = $include;

                        $this->setIncludes($temp);
                        continue;
                    }
//                    $this->unsetIncludes($tool, $version);
                }
            }
        }
    }

    public function build($filename, $params)
    {
        $includes = $this->getIncludes();
//        dd($includes);
//        $view = '';

        $queryBuilderConfig = require PACKAGE_CONFIGS_FOLDER . "/queryBuilder.php";

        if (count($params))
        {
            extract($params);
        }

//        $view = include( PACKAGE_CONFIG_VIEWS_FOLDER . "/" . ucfirst($filename) . "/" . $filename . ".php");
//        return $view->render();
//        dd($filename, $params, $includes, $queryBuilderConfig);



//        foreach ($includes as $tool => $version)
//        {
//            foreach ($version as $files)
//            {
//                foreach ($files as $html)
//                {
//                    $view .= $html;
//                }
//            }
//        }
//        $view .= file_get_contents(PACKAGE_CONFIG_VIEWS_FOLDER . "/" . ucfirst($filename) . "/" . $filename . ".php");

        ob_start();
        include(PACKAGE_CONFIG_VIEWS_FOLDER . "/" . ucfirst($filename) . "/" . $filename . ".php");
        $this->setView(ob_get_contents());
        return ob_end_clean();
    }
}