<?php
/**
 * Created by PhpStorm.
 * User: internship
 * Date: 04.03.2019
 * Time: 16:01
 */

namespace GMHanciu\ReportsPlatform\Report;


class DBConnection
{
    private $DBConfig;

    private $PDO;

    public function __construct()
    {
        $this->injectDBConfig();
        $this->start();
    }

    /**
     * @param array $DBConfig
     */
    public function setDBConfig(array $DBConfig): void
    {
        $this->DBConfig = $DBConfig;
    }

    /**
     * @return mixed
     */
    public function getDBConfig()
    {
        return $this->DBConfig;
    }

    /**
     * @param mixed $PDO|null
     */
    public function setPDO(\PDO $PDO = null): void
    {
        $this->PDO = $PDO;
    }

    /**
     * @return mixed
     */
    public function getPDO(): ?\PDO
    {
        return $this->PDO;
    }

    private function injectDBConfig()
    {
        $DBConfigPath = PROJECT_CONFIG_FOLDER . "/database.php";
        $config = require $DBConfigPath;
        $this->setDBConfig($config);
    }

    private function start()
    {
        $dsn = $this->DBConfig['DB_DATABASE_DRIVER']
            . ':host=' . $this->DBConfig['DB_HOST']
            . ';dbname=' . $this->DBConfig['DB_DATABASE']
            . ';port=' . $this->DBConfig['DB_PORT']
            . ';charset=' . $this->DBConfig['DB_CHARSET'];

        $user = $this->DBConfig['DB_USERNAME'];
        $pass = $this->DBConfig['DB_PASSWORD'];

        $options = $this->DBConfig['OPTIONS'];

        try
        {
            $this->setPDO(new \PDO($dsn, $user, $pass, $options));
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    protected function close()
    {
        $this->setPDO();
    }
}