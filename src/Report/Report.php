<?php

namespace GMHanciu\ReportsPlatform\Report;

use GMHanciu\ReportsPlatform\Helpers\File;
use PDO;
use RecursiveArrayIterator;

class Report extends Config
{

    private $tables = [];

    private $columnsByTables = [];

    public function __construct(String $name)
    {
        parent::__construct($name);
    }

    public function generate()
    {
        $config = $this->getConfig();

        if ($config)
        {
            return $this->actuallyGenerate($config);
        }
        else
        {
            return $this->create();
        }
    }

    private function actuallyGenerate(array $config)
    {
        return File::varExportMyWay($config);
    }

    private function create()
    {
        $tables = $this->getDBTables();
        $columnsByTables = $this->getDBColumnsByTables();

        $view = new View( 'create', compact( 'columnsByTables', 'tables' ) );

        return $view->render();
    }

    private function getDBTables()
    {
        $query = "SELECT table_name
                FROM information_schema.tables
                WHERE table_schema = (:DB_DATABASE)
                ORDER BY table_name";

        $query = $this->getPDO()->prepare($query);

        $query->bindParam(':DB_DATABASE', $this->getDBConfig()['DB_DATABASE']);

        $query->execute();

        $query->setFetchMode(PDO::FETCH_ASSOC);

        $results = [];

        foreach( (new RecursiveArrayIterator($query->fetchAll())) as $table_name )
        {
            $results[] = reset($table_name);
        }

        $this->setTables($results);

        return $this->getTables();
    }

    private function getDBColumnsByTables()
    {
        $query = "SELECT table_name, column_name
                FROM information_schema.columns
                WHERE table_schema = (:DB_DATABASE)
                ORDER BY table_name, ordinal_position";

        $query = $this->getPDO()->prepare($query);

        $query->bindParam(':DB_DATABASE', $this->getDBConfig()['DB_DATABASE']);

        $query->execute();

        $query->setFetchMode(PDO::FETCH_ASSOC);

        // we can already get all the tables from the previous query
        $results = $this->getTables();

        //flip the array because it is like $key => $table_name
        //we want the $table_name to be the key
        $results = array_flip($results);

        //fill all the keys with empty arrays as we're aiming for structure like
        //$table_name => [$columns]
        $results = array_fill_keys(array_keys($results), []);

        foreach( (new RecursiveArrayIterator($query->fetchAll())) as $columnWithTableName )
        {
            //reset $columnWithTableName = table_name
            //end $columnWithTableName = column_name
            $results[reset($columnWithTableName)][] = end($columnWithTableName);
        }

        $this->setColumnsByTables($results);

        return $this->getColumnsByTables();
    }

    /**
     * @param array $tables
     */
    public function setTables(array $tables): void
    {
        $this->tables = $tables;
    }

    /**
     * @return array
     */
    public function getTables(): array
    {
        return $this->tables;
    }

    /**
     * @param array $columnsByTable
     */
    public function setColumnsByTables(array $columnsByTable): void
    {
        $this->columnsByTables = $columnsByTable;
    }

    /**
     * @return array
     */
    public function getColumnsByTables(): array
    {
        return $this->columnsByTables;
    }


}