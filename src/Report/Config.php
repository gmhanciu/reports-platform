<?php

namespace GMHanciu\ReportsPlatform\Report;

class Config extends DBConnection
{

    private $createPage = '';
    private $editPage = '';
    private $config = null;

    public function __construct(String $name)
    {
        parent::__construct();

        if ($this->configExists($name))
        {
            $fullConfigPath = $this->fullPath($name, 'config');
            $config = require $fullConfigPath;
            $this->setConfig($config);
        }
//        else
//        {
//            $this->setCreatePage(
//                $this->inject($this->fullPath('create', 'view'), 'create')
//            );
//        }
    }

    private function inject(String $path, String $type)
    {
        switch ($type)
        {
            case 'create':
//                return require $path;
                return (new View())->build('create');
                break;
            case 'edit':
                break;
        }
    }

    private function fullPath(String $filename, String $type): String
    {
        switch ($type)
        {
            case 'config':
                return PROJECT_REPORTS_CONFIG_FOLDER . "/" . $filename . '.php';
                break;
            case 'view':
                return PACKAGE_CONFIG_VIEWS_FOLDER . "/" . ucfirst($filename) . "/" . $filename . '.php';
                break;
        }
    }

    private function configExists(String $filename): bool
    {
        $file = $this->fullPath($filename, 'config');

        return is_file($file);
    }

    /**
     * @param mixed $config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    public function getConfig(): ?array
    {
        return $this->config;
    }

    /**
     * @param string $createPage
     */
    public function setCreatePage(string $createPage): void
    {
        $this->createPage = $createPage;
    }

    /**
     * @return string
     */
    public function getCreatePage(): string
    {
        return $this->createPage;
    }
}