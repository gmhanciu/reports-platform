<?php

return [
    'join' => [
        'INNER JOIN',
        'LEFT OUTER JOIN',
        'RIGHT OUTER JOIN',
        'FULL OUTER JOIN',
        'CROSS JOIN',
    ],
    'select' => [
        'Functions' => [
            'char_length',
            'date',
            'from_unixtime',
            'lower',
            'round',
            'sec_to_time',
            'time_to_sec',
            'upper',
        ],
        'Aggregation' => [
            'avg',
            'count',
            'count_distinct',
            'group_concat',
            'max',
            'min',
            'sum',
        ],
    ],
    'where' => [
        '=',
        '<',
        '>',
        '<=',
        '>=',
        '!=',
        'LIKE',
        'LIKE %%',
        'REGEXP',
        'IN',
        'IS NULL',
        'NOT LIKE',
        'NOT REGEXP',
        'NOT IN',
        'IS NOT NULL',
        'SQL'
    ],
    'sort' => [
        'ascending',
        'descending'
    ],
];